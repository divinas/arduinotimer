#include "ArduinoTimer.hpp"
#include "Arduino.h"

float clamp(float d, float min, float max) {
  const float t = d < min ? min : d;
  return t > max ? max : t;
}

void ArduinoTimer::Start(unsigned long duration)
{
    m_StartTime = millis();
    m_IsStarted = true;
    m_Duration = duration;
}

void ArduinoTimer::Stop()
{
    m_IsStarted = false;
}

bool ArduinoTimer::IsElapsed() const
{
    return GetElapsedTime() > m_Duration;
}

bool ArduinoTimer::IsStarted() const
{
    return m_IsStarted;
}

unsigned long ArduinoTimer::GetElapsedTime() const
{
  return millis() - m_StartTime;
}

float ArduinoTimer::GetElapsedRatio() const
{
  return clamp(GetElapsedTime() / (float)m_Duration, 0.f, 1.f);
}
