#ifndef ARDUINO_TIMER_HPP
#define ARDUINO_TIMER_HPP
class ArduinoTimer
{
public:
    ArduinoTimer() : m_IsStarted(false){}
    void Start(unsigned long duration);
    void Stop();
    bool IsElapsed() const;
    bool IsStarted()const ;
    unsigned long GetElapsedTime() const;
    float GetElapsedRatio() const;

private:
    unsigned long m_StartTime;
    unsigned long m_Duration;
    bool m_IsStarted;
};
#endif //ARDUINO_TIMER_H
